#! /usr/bin/python3

# SPDX-License-Identifier: LGPL-2.1-or-later

import argparse
import base64
import json
import sys
import yaml

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Concert TPM2 eventlog YAML to UEFI profile JSON')
    parser.add_argument('eventlog',help='TPM2 eventlog (e.g. /sys/kernel/security/tpm0/binary_bios_measurements)')
    parser.add_argument('-o', '--output', help='Where to save UEFI profile JSON')
    args = parser.parse_args()

    res = {}

    with open(args.eventlog, 'r') as fp:
        y = yaml.safe_load(fp.read())
        for event in y['events']:
            if 'PCRIndex' not in event or 'EventType' not in event:
                continue
            if not event['PCRIndex'] == 7 or event['EventType'] != 'EV_EFI_VARIABLE_DRIVER_CONFIG':
                continue
            if 'Event' not in event:
                continue
            event_inner = event['Event']
            if 'UnicodeName' not in event_inner or 'VariableData' not in event_inner:
                continue
            if event_inner['UnicodeName'] in ['db', 'dbx', 'KEK', 'PK']:
                data = bytearray.fromhex(event_inner['VariableData'])
                res[event_inner['UnicodeName']] = base64.b64encode(data).decode('ascii')

    if set(res.keys()) != {'db', 'dbx', 'PK', 'KEK'}:
        print("Mallformed eventlog!", file=sys.stderr)
        sys.exit(1)

    res_out = json.dumps(res, indent=2)
    if args.output:
        with open(args.output, 'w') as fp:
            fp.write(res_out)
    else:
        print(res_out)
