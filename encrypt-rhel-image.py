#! /usr/bin/python3

# SPDX-License-Identifier: LGPL-2.1-or-later

import argparse
import glob
import hashlib
import io
import sys
import subprocess
import secrets
import string
import json
import re
import socket
import os
import base64
import time
import stat
import ctypes
from uuid import UUID

GUID_GLOBAL_VARIABLE = UUID("8be4df61-93ca-11d2-aa0d-00e098032b8c")
GUID_SECURITY_DATABASE = UUID("d719b2cb-3d3a-4596-a3bc-dad00e67656f")
GUID_SHIM_LOCK = UUID("605dab50-e046-4300-abb6-3dd810dd8b23")

EFI_CERT_SHA1_GUID   = UUID("826ca512-cf10-4ac9-b187be01496631bd")
EFI_CERT_SHA224_GUID = UUID("0b6e5233-a65c-44c9-9407d9ab83bfc8bd")
EFI_CERT_SHA256_GUID = UUID("c1c41626-504c-4092-aca941f936934328")
EFI_CERT_SHA384_GUID = UUID("ff3e5307-9fd0-48c9-85f18ad56c701e01")
EFI_CERT_SHA512_GUID = UUID("093e0fae-a6c4-4f50-9f1bd41e2b89c19a")
EFI_CERT_TYPE_X509_GUID = UUID("a5c059a1-94e4-4aa7-87b5ab155c2bf072")

MICROSOFT_GUID = UUID("77fa9abd-0359-4d32-bd60-28f4e78f784b")

# Borrowed from https://gitlab.com/berrange/tpm-prevision/
class PEImage:

    # https://learn.microsoft.com/en-us/windows/win32/debug/pe-format
    # https://upload.wikimedia.org/wikipedia/commons/1/1b/Portable_Executable_32_bit_Structure_in_SVG_fixed.svg

    # Magic bytes 'MZ'
    DOS_SIG = bytes([0x4d, 0x5a])

    # Offset in DOS header where the PE offset live
    DOS_HEADER_FIELD_PE_OFFSET = 0x3c

    # Magic bytes 'PE\0\0'
    PE_SIG = bytes([0x50, 0x45, 0x00, 0x00])

    # Offsets relative to start of PE_SIG
    PE_HEADER_FIELD_MACHINE = 0x4
    PE_HEADER_FIELD_NUMBER_OF_SECTIONS = 0x6
    PE_HEADER_FIELD_POINTER_TO_SYMBOL_TABLE = 0xc
    PE_HEADER_FIELD_NUMBER_OF_SYMBOLS = 0x10
    PE_HEADER_FIELD_SIZE_OF_OPTIONAL_HEADER = 0x14
    PE_HEADER_SIZE = 0x18

    MACHINE_I686 = 0x014c
    MACHINE_X86_64 = 0x8664
    MACHINE_AARCH64 = 0xaa64

    MACHINES = [MACHINE_I686,
                MACHINE_X86_64,
                MACHINE_AARCH64]


    PE32_SIG = bytes([0x0b, 0x01])
    PE32P_SIG = bytes([0x0b, 0x02])

    # Offsets relative to PE_HEADER_SIZE
    PE32X_HEADER_FIELD_SIZE_OF_HEADERS = 0x3c
    PE32X_HEADER_FIELD_CHECKSUM = 0x40
    PE32_HEADER_FIELD_CERT_TABLE = 0x80
    PE32P_HEADER_FIELD_CERT_TABLE = 0x90

    SECTION_HEADER_FIELD_VIRTUAL_SIZE = 0x8
    SECTION_HEADER_FIELD_SIZE = 0x10
    SECTION_HEADER_FIELD_OFFSET = 0x14
    SECTION_HEADER_LENGTH = 0x28

    def __init__(self, path=None, data=None):
        self.path = path
        self.data = data
        self.sections = {}
        self.authenticodehash = None
        self.certchains = []
        self.load()

    def has_section(self, name):
        return name in self.sections

    def get_section_data(self, name):
        if name not in self.sections:
            raise Exception("Missing section '%s'" % name)

        start, end = self.sections[name]
        with open(self.path, 'rb') as fp:
            fp.seek(start)
            return fp.read(end-start)

    def get_authenticode_hash(self):
        return self.authenticodehash

    def load(self):
        if self.path is not None:
            with open(self.path, 'rb') as fp:
                self.load_fh(fp)
        elif self.data is not None:
            with io.BytesIO(self.data) as fp:
                self.load_fh(fp)
        else:
            raise Exception("Either path or data is required")

    def load_fh(self, fp):
        dossig = fp.read(2)
        if dossig != self.DOS_SIG:
            raise Exception("Missing DOS header magic %s got %s" % (self.DOS_SIG, dossig))

        fp.seek(self.DOS_HEADER_FIELD_PE_OFFSET)
        peoffset = int.from_bytes(fp.read(4), 'little')
        fp.seek(peoffset)

        pesig = fp.read(4)
        if pesig != self.PE_SIG:
            raise Exception("Missing PE header magic %s at %x got %s" % (
                self.PE_SIG, peoffset, pesig))

        fp.seek(peoffset + self.PE_HEADER_FIELD_MACHINE)
        pemachine = int.from_bytes(fp.read(2), 'little')

        if pemachine not in self.MACHINES:
            raise Exception("Unexpected PE machine architecture %x expected [%s]" %
                            (pemachine, ", ".join(["%x" % m for m in self.MACHINES])))

        fp.seek(peoffset + self.PE_HEADER_FIELD_NUMBER_OF_SECTIONS)
        numOfSections = int.from_bytes(fp.read(2), 'little')

        fp.seek(peoffset + self.PE_HEADER_FIELD_SIZE_OF_OPTIONAL_HEADER)
        sizeOfOptionalHeader = int.from_bytes(fp.read(2), 'little')

        fp.seek(peoffset + self.PE_HEADER_FIELD_POINTER_TO_SYMBOL_TABLE)
        pointerToSymbolTable = int.from_bytes(fp.read(4), 'little')

        fp.seek(peoffset + self.PE_HEADER_FIELD_NUMBER_OF_SYMBOLS)
        numberOfSymbols = int.from_bytes(fp.read(4), 'little')

        # image loader header follows PE header
        fp.seek(peoffset + self.PE_HEADER_SIZE)

        ldrsig = fp.read(2)
        if pemachine == self.MACHINE_I686:
            wantldrsig = self.PE32_SIG
            certtableoffset = self.PE32_HEADER_FIELD_CERT_TABLE
        else:
            wantldrsig = self.PE32P_SIG
            certtableoffset = self.PE32P_HEADER_FIELD_CERT_TABLE

        if ldrsig != wantldrsig:
            raise Exception("Missing image loader signature %s got %s" % (
                wantldrsig.hex(), ldrsig.hex()))

        # Extract all PKCS7 objects from cert table
        certchains = []

        fp.seek(peoffset + self.PE_HEADER_SIZE + self.PE32X_HEADER_FIELD_SIZE_OF_HEADERS)
        sizeOfHeaders = int.from_bytes(fp.read(4), 'little')

        fp.seek(peoffset + self.PE_HEADER_SIZE + certtableoffset)
        certTableOffset = int.from_bytes(fp.read(4), 'little')

        fp.seek(peoffset + self.PE_HEADER_SIZE + certtableoffset + 4)
        certTableSize = int.from_bytes(fp.read(4), 'little')

        offset = 0
        while offset < certTableSize:
            fp.seek(certTableOffset + offset)
            certlen = int.from_bytes(fp.read(4), 'little')
            certrev = int.from_bytes(fp.read(2), 'little')
            certtype = int.from_bytes(fp.read(2), 'little')
            if certtype == 2 and certrev == 0x200 and certlen:
                pkcs7 = fp.read(certlen - 8)
                certchains.append(pkcs7)
            offset += ((offset + certlen + 7) // 8) * 8

        # When hashing the file, we need to exclude certain areas
        # with variable data as detailed in:
        #
        # https://reversea.me/index.php/authenticode-i-understanding-windows-authenticode/
        #
        # so build a list of start/end offsets to hash over
        tohash = [
            # From start of file, to the checksum
            [0, peoffset + self.PE_HEADER_SIZE + self.PE32X_HEADER_FIELD_CHECKSUM],

            # From after checksum, to the certificate table
            [peoffset + self.PE_HEADER_SIZE + self.PE32X_HEADER_FIELD_CHECKSUM + 4,
             peoffset + self.PE_HEADER_SIZE + certtableoffset],

            # From after certificate table to end of headers
            [peoffset + self.PE_HEADER_SIZE + certtableoffset + 8,
             sizeOfHeaders],
        ]

        imageDataLength = sizeOfHeaders
        nextSection = peoffset + self.PE_HEADER_SIZE + sizeOfOptionalHeader
        for i in range(numOfSections):
            fp.seek(nextSection)
            name = fp.read(8).decode('ascii').rstrip('\0')

            if len(name) and name[0] == '/':
                # This is a long name, must check strings table. String table is located
                # right after symbols table, every entry in symbols table is 18 bytes long.
                string_offset = pointerToSymbolTable + (numberOfSymbols * 18)
                string_offset += int(name[1:])
                fp.seek(string_offset)
                name = ""
                while True:
                    b = fp.read(1)
                    if len(b) and b != b'\x00':
                        name += b.decode('ascii')
                    else:
                        break

            fp.seek(nextSection + self.SECTION_HEADER_FIELD_OFFSET)
            sectionOffset = int.from_bytes(fp.read(4), 'little')
            fp.seek(nextSection + self.SECTION_HEADER_FIELD_SIZE)
            sectionSize = int.from_bytes(fp.read(4), 'little')
            fp.seek(nextSection + self.SECTION_HEADER_FIELD_VIRTUAL_SIZE)
            sectionVirtualSize = int.from_bytes(fp.read(4), 'little')

            if sectionSize != 0:
                tohash.append([sectionOffset, sectionOffset + sectionSize])
                imageDataLength += sectionSize
                self.sections[name] = [sectionOffset, sectionOffset + sectionVirtualSize]
            nextSection += self.SECTION_HEADER_LENGTH


        fileLength = fp.seek(0, 2) - certTableSize

        if imageDataLength < fileLength:
            tohash.append([imageDataLength, fileLength])

        tohash = sorted(tohash, key=lambda r: r[0])

        h = hashlib.new('sha256')
        for area in tohash:
            fp.seek(area[0])
            h.update(fp.read(area[1]-area[0]))

        self.certchains = certchains
        self.authenticodehash = h.digest()

# Convert Azure disk profile to EFI Signature Lists (ESLs)
class AZDiskProfile:
    uefi = {}

    def __init__(self, path):
        profile = json.loads(open(path, 'r').read())
        for var in ['PK', 'KEK', 'db', 'dbx']:
            self.uefi[var] = base64.b64encode(self.build_esl(profile["properties"]["uefiSettings"]["signatures"][var])).decode('ascii')

    # Each x509 gets its own list as it is unlikely that different x509s have the same size
    def build_esl_x509(self, sigvalues):
        res = bytearray()
        for sig in sigvalues:
            sigbytes = base64.b64decode(sig)
            # SignatureType
            res.extend(EFI_CERT_TYPE_X509_GUID.bytes_le)
            # SignatureOwner == 16 bytes + header
            res.extend((len(sigbytes) + 16 + 28).to_bytes(4, 'little'))
            # SignatureHeaderSize
            res.extend((0).to_bytes(4, 'little'))
            # SignatureSize
            res.extend((len(sigbytes) + 16).to_bytes(4, 'little'))
            # MICROSOFT_GUID
            res.extend(MICROSOFT_GUID.bytes_le)
            # Cert
            res.extend(sigbytes)
        return res

    # Sha hashes have the same size and appear as a list
    def build_esl_sha(self, sigvalues, guid):
        res = bytearray()

        if len(sigvalues) == 0:
            return res

        siglen = len(base64.b64decode(sigvalues[0]))
        if guid == EFI_CERT_SHA1_GUID and siglen != 20:
            raise Exception("SHA1 signatures must be 20 bytes long")
        elif guid == EFI_CERT_SHA224_GUID and siglen != 28:
            raise Exception("SHA224 signatures must be 28 bytes long")
        elif guid == EFI_CERT_SHA256_GUID and siglen != 32:
            raise Exception("SHA256 signatures must be 32 bytes long")
        elif guid == EFI_CERT_SHA384_GUID and siglen != 48:
            raise Exception("SHA384 signatures must be 48 bytes long")
        elif guid == EFI_CERT_SHA512_GUID and siglen != 64:
            raise Exception("SHA512 signatures must be 64 bytes long")

        # SignatureType
        res.extend(guid.bytes_le)
        # SignatureOwner == 16 bytes to each signature
        res.extend(((siglen + 16) * len(sigvalues) + 28).to_bytes(4, 'little'))
        # SignatureHeaderSize
        res.extend((0).to_bytes(4, 'little'))
        # SignatureSize
        res.extend((siglen + 16).to_bytes(4, 'little'))

        for sig in sigvalues:
            sigbytes = base64.b64decode(sig)
            # MICROSOFT_GUID
            res.extend(MICROSOFT_GUID.bytes_le)
            # Cert
            if len(sigbytes) != siglen:
                raise Exception("All SHA signatures must have the same length")
            res.extend(sigbytes)
        return res

    def build_esl(self, signatures):
        res = bytearray()
        # PK contains only one signature
        if type(signatures) != list:
            signatures = [signatures]

        for signature in signatures:
            sigtype = signature["type"]
            sigvalues = signature["value"]
            if sigtype == "x509":
                res.extend(self.build_esl_x509(sigvalues))
            elif sigtype == "sha1":
                res.extend(self.build_esl_sha(sigvalues, EFI_CERT_SHA1_GUID))
            elif sigtype == "sha224":
                res.extend(self.build_esl_sha(sigvalues, EFI_CERT_SHA224_GUID))
            elif sigtype == "sha256":
                res.extend(self.build_esl_sha(sigvalues, EFI_CERT_SHA256_GUID))
            elif sigtype == "sha384":
                res.extend(self.build_esl_sha(sigvalues, EFI_CERT_SHA384_GUID))
            elif sigtype == "sha512":
                res.extend(self.build_esl_sha(sigvalues, EFI_CERT_SHA512_GUID))
            else:
                raise Exception("Sigtype %s is unsupported!" % sigtype)
        return res

def efivars_read(path, no_attrs):
    uefi = {}

    for var, guid in [('PK', GUID_GLOBAL_VARIABLE), ('KEK', GUID_GLOBAL_VARIABLE), ('db', GUID_SECURITY_DATABASE), ('dbx', GUID_SECURITY_DATABASE)]:
        filename = f'{path}/{var}-{guid}'
        try:
            with open(filename, 'br') as f:
                data = f.read()
        except:
            raise Exception(f'Could not read efivars file: {filename}')
        if not no_attrs:
            # efivars files from efivarfs contain a 4-byte header with the var attrs that we need to skip over
            data = data[4:]
        uefi[var] = base64.b64encode(data)

    return uefi

def extend_pcr(pcr, digest, verbose, log_prefix):
    if verbose:
        print(log_prefix, ''.join('{:02x}'.format(x) for x in digest))
    return hashlib.sha256(pcr + digest).digest()

def event_hash(guid, name, value):
    d = guid.bytes_le
    d += len(name).to_bytes(8, 'little')
    d += len(value).to_bytes(8, 'little')
    d += name.encode('utf-16-le')
    d += value
    return hashlib.sha256(d).digest()

def is_sha265(value):
    result = None
    try: result = re.match(r'^(0x|)\w{64}$', value).group(0)
    except: pass
    return result is not None

def pkcs7_subject_issuer(binary):
    certs_list = []
    for pkcs7 in binary.certchains:
        res = run_command(["openssl", "pkcs7", "-inform", "der", "-print_certs"], input=pkcs7, text=False)
        certs_list.extend(re.findall("^subject=(.*)(?:\n)+^issuer=(.*)\n", res.stdout.decode('ascii'), re.MULTILINE))
    return certs_list

def get_boot_components(args, espmpath):
    shimname = 'shim'
    bootloaderpath = None
    if os.path.exists("%s/EFI/redhat" % espmpath):
        vendor='redhat'
    elif os.path.exists("%s/EFI/fedora" % espmpath):
        vendor='fedora'
    elif os.path.exists("%s/EFI/azurelinux" % espmpath):
        vendor='azurelinux'
    else:
        print("No EFI vendor dir, assuming legacy Mariner 2 layout")
        vendor='BOOT'
        shimname='boot'

    # Check is shim exists
    shimpath = "%s/EFI/%s/%sx64.efi" % (espmpath, vendor, shimname)
    if not os.path.exists(shimpath):
        raise Exception("/EFI/%s/%sx64.efi can't be found!" % (vendor, shimname))

    # Try getting UKI path from shim fallback
    ukipath = None
    if os.path.exists("%s/EFI/%s/BOOTX64.CSV" % (espmpath, vendor)):
        # BOOTX64.CSV may contain several entries, take the last one as it will be the first in boot order
        ukipath = open("%s/EFI/%s/BOOTX64.CSV" % (espmpath, vendor), encoding='utf-16-le').readlines()[-1]
        ukipath = ukipath.split(',')[2].split(' ')[0]
        if ukipath.lower().endswith(".efi"):
            ukipath = re.sub("\\\\", "/", ukipath)
            if not os.path.exists("%s/%s" % (espmpath, ukipath)):
                print("UKI %s from shim fallback doesn't exist!" % ukipath)
                ukipath = None
            else:
                print("Using UKI %s from shim fallback for PCR prediction" % ukipath)
                ukipath = "%s/%s" % (espmpath, ukipath)
        else:
            print("BOOTX64.CSV does not set UKI to boot")
            ukipath = None

    # If UKI is not set in BOOTX64.CSV, shim will attempt to load grubx64.efi
    if ukipath is None:
        bootloaderpath = "%s/EFI/%s/grubx64.efi" % (espmpath, vendor)
        if not os.path.exists(bootloaderpath):
            raise Exception("No UKI set in BOOTX64.CSV and grubx64.efi is missing!")
        print("Using 2nd stage bootloader /EFI/%s/grubx64.efi" % vendor)

        ukis = sorted(glob.glob("%s/EFI/Linux/*.efi" % espmpath))
        if ukis != []:
            ukipath = ukis[-1]
            print("Warning! Using the latest UKI %s for PCR prediction" % ukipath[len(espmpath):])
    if ukipath is None:
        raise Exception("No UKI found for PCR prediction!")

    return shimpath, bootloaderpath, ukipath

def predict_pcr4(args, espmpath):
    verbose = args.verbose

    shimpath, bootloaderpath, ukipath = get_boot_components(args, espmpath)

    # Calculate the expected PCR4 value
    pcr4 = bytearray.fromhex('0000000000000000000000000000000000000000000000000000000000000000')
    if verbose:
        print("PCR4 prediction started: 0000000000000000000000000000000000000000000000000000000000000000")
    # EV_EFI_ACTION
    pcr4 = extend_pcr(pcr4, hashlib.sha256(bytearray(b"Calling EFI Application from Boot Option")).digest(),
                      verbose, "PCR4: extending 'Calling EFI Application from Boot Option', hash")
    # EV_SEPARATOR
    pcr4 = extend_pcr(pcr4, hashlib.sha256(bytearray.fromhex("00000000")).digest(),
                      verbose, "PCR4: extending '00000000' SEPARATOR, hash")
    # shim
    shim = PEImage(path = shimpath)
    pcr4 = extend_pcr(pcr4, shim.get_authenticode_hash(), verbose, "PCR4: extending shim's hash")

    # Second stage bootloader if needed (e.g. sd-boot)
    if bootloaderpath is not None:
        bootloader = PEImage(path = bootloaderpath)
        pcr4 = extend_pcr(pcr4, bootloader.get_authenticode_hash(), verbose, "PCR4: extending bootloader's hash")

    # UKI
    uki = PEImage(path = ukipath)
    pcr4 = extend_pcr(pcr4, uki.get_authenticode_hash(), verbose, "PCR4: extending UKI's hash")
    # With SecureBoot disabled, measure ".linux" section too
    if args.nosecureboot:
        linux = PEImage(data=uki.get_section_data(".linux"))
        pcr4 = extend_pcr(pcr4, linux.get_authenticode_hash(), verbose, "PCR4: extending Linux's hash")

    return '0x' + ''.join('{:02x}'.format(x) for x in pcr4).upper()

def get_db_certs(db):
    certs = []
    offset = 0

    while offset < len(db) - 28:
        list_type = db[offset:offset + 16]
        list_size = int.from_bytes(db[offset + 16:offset + 20], 'little')
        head_size = int.from_bytes(db[offset + 20:offset + 24], 'little')
        item_size = int.from_bytes(db[offset + 24:offset + 28], 'little')

        if offset + list_size > len(db):
            raise Exception("Invalid list size!")

        offset += (28 + head_size)
        item_offset = 0

        if list_type == EFI_CERT_TYPE_X509_GUID.bytes_le:
            while item_offset < list_size - (head_size + 28):
                item = db[offset + item_offset:offset + item_offset + item_size]
                res = run_command(["openssl", "x509", "-inform", "der", "-subject", "-issuer"], input=item[16:], text=False)

                for subject, issuer in re.findall("^subject=(.*)\n^issuer=(.*)", res.stdout.decode('ascii'), re.MULTILINE):
                    certs.append({"subject": subject, "issuer": issuer, "bytes": item})

                item_offset += item_size

        offset += list_size - (28 + head_size)

    return certs

def find_cert_in_db(binary, db):
    x509_certs = get_db_certs(db)
    for subject_bin, issuer_bin in pkcs7_subject_issuer(binary):
        # Compare CNs only!
        for x509_cert in x509_certs:
            # The same or parent
            if x509_cert['subject'] in [subject_bin, issuer_bin]:
                return x509_cert['bytes']

    return None

def shim_read_vendor_auth(shim):
    if not shim.has_section(".vendor_cert"):
        return None

    data = io.BytesIO(shim.get_section_data(".vendor_cert"))

    try:
        auth_size = int.from_bytes(data.read(4), 'little')
        deauth_size = int.from_bytes(data.read(4), 'little')
        auth_off_t = int.from_bytes(data.read(4), 'little')
        deauth_off_t = int.from_bytes(data.read(4), 'little')

        data.seek(auth_off_t)

        return data.read(auth_size)
    except:
        print("Unsupported shim .vendor_cert section format")
        return None

def shim_vendor_cert_is_x509(vendor_cert):
    # .vendor_cert can either be a DER encoded cert or DB, try decoding DER with openssl
    res = run_command(["openssl", "x509", "-inform", "der", "-subject", "-issuer"], input=vendor_cert, text=False, canfail=True)
    if res.returncode != 0:
        return None
    return res.stdout.decode('ascii')

def find_cert_in_shim_vendor_cert(binary, shim):
    vc = shim_read_vendor_auth(shim)
    if not vc:
        return None

    cert = shim_vendor_cert_is_x509(vc)
    if not cert:
        return None

    for subject, issuer in re.findall("^subject=(.*)\n^issuer=(.*)", cert, re.MULTILINE):
	# Compare CNs only!
        for subject_bin, issuer_bin in pkcs7_subject_issuer(binary):
            # The same or parent
            if subject in [subject_bin, issuer_bin]:
                return vc
    return None

def find_cert_in_shim_vendor_db(binary, shim):
    db = shim_read_vendor_auth(shim)
    if not db:
        return None

    if shim_vendor_cert_is_x509(db):
        return None

    return find_cert_in_db(binary, db)

def get_sbat_latest(sbat):
    magic = int.from_bytes(sbat.read(4), 'little')
    if magic != 0:
        return None

    off_t_prev = int.from_bytes(sbat.read(4), 'little')
    off_t_latest = int.from_bytes(sbat.read(4), 'little')

    # account for magic
    sbat.seek(off_t_prev + 4)

    # 'latest' is the last section, read untill the end
    sbat_bytes = sbat.read()

    # string must be null terminated
    if sbat_bytes.find(0) != -1:
        return sbat_bytes[0:sbat_bytes.find(0)]

def get_measures_moklisttrusted_to_pcr7(shim):
    # Shim before 15.7 was wrongly measuring "MokListTrusted: 01" into PCR7, see
    # https://github.com/rhboot/shim/issues/484
    # Use shim's .sbat section to detect older version. Note, nothing older that
    # 15.5 was ever supported by this tool so just try our best to detect 15.5 and
    # 15.6.
    if not shim.has_section(".sbat"):
        return False
    try:
        sbat = shim.get_section_data(".sbat").decode('ascii').split('\n')
        for sbat_line in sbat:
            entries = sbat_line.split(',')
            if len(entries) < 5:
                continue
            if entries[0].strip() == 'shim.redhat' and entries[4].strip() in ['15.5', '15.6']:
                return True
    except:
        pass

    return False

def predict_pcr7(args, espmpath):
    verbose = args.verbose
    shimpath, bootloaderpath, ukipath = get_boot_components(args, espmpath)

    pcr7 = bytearray.fromhex('0000000000000000000000000000000000000000000000000000000000000000')
    if verbose:
        print("PCR7 prediction started: 0000000000000000000000000000000000000000000000000000000000000000")
    if args.efivars_profile:
        uefi = efivars_read(args.efivars_profile, args.efivars_profile_no_attrs)
    elif args.uefi_profile:
        uefi = json.loads(open(args.uefi_profile, 'r').read())
    elif args.az_disk_profile:
        uefi = AZDiskProfile(args.az_disk_profile).uefi
    else:
        raise("Either UEFI profile or Azure disk profile is required for PCR7 prediction")

    shim = PEImage(path = shimpath)
    if bootloaderpath is not None:
        bootloader = PEImage(path = bootloaderpath)
    else:
        bootloader = None
    uki = PEImage(path = ukipath)

    # SecureBoot state
    pcr7 = extend_pcr(pcr7, event_hash(GUID_GLOBAL_VARIABLE, "SecureBoot", bytes.fromhex('01')),
                      verbose, "PCR7: extending 'SecureBoot: 01', hash")

    # PK, KEK, db, dbx
    pcr7 = extend_pcr(pcr7, event_hash(GUID_GLOBAL_VARIABLE, "PK", base64.b64decode(uefi['PK'])),
                      verbose, "PCR7: extending 'PK' variable, hash")
    pcr7 = extend_pcr(pcr7, event_hash(GUID_GLOBAL_VARIABLE, "KEK", base64.b64decode(uefi['KEK'])),
                      verbose, "PCR7: extending 'KEK' variable, hash")
    pcr7 = extend_pcr(pcr7, event_hash(GUID_SECURITY_DATABASE, "db", base64.b64decode(uefi['db'])),
                      verbose, "PCR7: extending 'db' variable, hash")
    pcr7 = extend_pcr(pcr7, event_hash(GUID_SECURITY_DATABASE, "dbx", base64.b64decode(uefi['dbx'])),
                      verbose, "PCR7: extending 'dbx' variable, hash")

    # EV_SEPARATOR
    pcr7 = extend_pcr(pcr7, hashlib.sha256(bytearray.fromhex("00000000")).digest(),
                      verbose, "PCR7: extending '00000000' SEPARATOR, hash")

    # In case several boot components are signed by the same certificate, it is only measured once
    certs_used = []

    # Certificate which signed shim
    shim_cert_in_db = find_cert_in_db(shim, base64.b64decode(uefi['db']))
    if shim_cert_in_db:
        pcr7 = extend_pcr(pcr7, event_hash(GUID_SECURITY_DATABASE, "db", shim_cert_in_db),
                          verbose, "PCR7: shim's signator found in db, extending the matching cert, hash")
        certs_used.append(event_hash(GUID_SECURITY_DATABASE, "db", shim_cert_in_db))
    else:
        raise Exception("SecureBoot certificate which signed Shim can't be found in db!")

    if shim.has_section(".sbatlevel"):
        sbat_bytes = get_sbat_latest(io.BytesIO(shim.get_section_data(".sbatlevel")))
        pcr7 = extend_pcr(pcr7, event_hash(GUID_SHIM_LOCK, "SbatLevel", sbat_bytes),
                      verbose, "PCR7: extending '.sbatlevel' from shim's binary, hash")
    else:
        # Fingers crossed it's sbat,1,2021030218
        pcr7 = extend_pcr(pcr7, event_hash(GUID_SHIM_LOCK, "SbatLevel", bytearray(b'sbat,1,2021030218\n')),
                          verbose, "PCR7: extending '.sbatlevel' 'sbat,1,2021030218', hash")

    if get_measures_moklisttrusted_to_pcr7(shim):
        # MokListTrusted
        pcr7 = extend_pcr(pcr7, event_hash(GUID_SHIM_LOCK, "MokListTrusted", bytes.fromhex('01')),
                          verbose, "PCR7: extending 'MokListTrusted: 01', hash")

    # Shim either boots UKI directly or through second state bootloader
    binaries = []
    if bootloader:
        binaries.append((bootloader, "bootloader"))
    binaries.append((uki, "UKI"))

    for binary, binary_name in binaries:
        # Is the certificate which signed the binary in 'db'?
        bin_cert_in_db = find_cert_in_db(binary, base64.b64decode(uefi['db']))
        if bin_cert_in_db:
            ehash = event_hash(GUID_SECURITY_DATABASE, "db", bin_cert_in_db)
            if ehash not in certs_used:
                pcr7 = extend_pcr(pcr7, ehash, verbose,
                                  "PCR7: %s's signator found in db, extending the matching cert, hash" % binary_name)
                certs_used.append(ehash)
            elif verbose:
                print("PCR7: %s's signator found in db but the hash is already used, skipping" % binary_name)
            continue

        # Is the certificate which signed the binary is shim's 'vendor_db'?
        vendor_db_cert = find_cert_in_shim_vendor_db(binary, shim)
        if vendor_db_cert:
            ehash = event_hash(GUID_SECURITY_DATABASE, "vendor_db", vendor_db_cert)
            if ehash not in certs_used:
                pcr7 = extend_pcr(pcr7, ehash, verbose,
                                  "PCR7: %s's signator found in 'vendor_db', extending the matching cert, hash" % binary_name)
                certs_used.append(ehash)
            elif verbose:
                print("PCR7: %s's signator found in 'vendor_db' but the hash is already used, skipping" % binary_name)
            continue

        # Is the certificate which signed the binary is shim's 'vendor_cert'?
        vendor_cert = find_cert_in_shim_vendor_cert(binary, shim)
        if vendor_cert:
            # shim 15.7+ extends MokListRT and uses it before checking .vendor_cert,
            # which in practice means that MokListRT entry always gets logged.
            # See: https://github.com/rhboot/shim/issues/714
            ehash = event_hash(GUID_SHIM_LOCK, "MokListRT", GUID_SHIM_LOCK.bytes_le + vendor_cert)
            if ehash not in certs_used:
                pcr7 = extend_pcr(pcr7, ehash, verbose,
                                  "PCR7: %s's signator found in 'vendor_cert', extending the matching cert, hash" % binary_name)
                certs_used.append(ehash)
            elif verbose:
                print("PCR7: %s's signator found in 'vendor_cert' but the hash is already used, skipping" % binary_name)
            continue

        # Certificate was not found
        raise Exception("SecureBoot certificate which signed %s can't be found in db/shim!" % binary_name)

    return '0x' + ''.join('{:02x}'.format(x) for x in pcr7).upper()

# Get two consecutive free ports, racy as we close them :-(
def get_two_free_ports():
    while True:
        found = False
        port1 = secrets.choice(range(1025, 65536))
        try:
            sock1 = socket.socket()
            sock2 = socket.socket()
            sock1.bind(('127.0.0.1', port1))
            sock2.bind(('127.0.0.1', port1+1))
            found = True
        except:
            pass
        finally:
            sock1.close()
            sock2.close()
        if found:
            return (port1, port1+1)

def randpw(len):
    return ''.join([secrets.choice(string.ascii_letters + string.digits) for k in range(len)])

def get_pcrs_dat(pcrs):
    bytearr = []
    assert(pcrs['bank'][0:3] == 'sha')
    bytescnt = int(pcrs['bank'][3:]) // 8
    for pcr in pcrs['pcrs']:
        if pcrs[str(pcr)][0:2] == '0x':
            pcrval = pcrs[str(pcr)][2:]
        else:
            pcrval = pcrs[str(pcr)]
        for bytenum in range(0, bytescnt):
            bytearr.append(int(pcrval[bytenum * 2: bytenum * 2 + 2], 16))
    return bytearray(bytearr)

def run_command(cmdargs, sysexit=False, canfail=False, input=None, text=True):
    res = subprocess.run(cmdargs, check=False, capture_output=True, text=text, input=input)
    if res.returncode != 0 and not canfail:
        print("%s returned %d, stdout: %s stderr: %s" % (res.args, res.returncode, res.stdout, res.stderr), file=sys.stderr)
        if sysexit:
            sys.exit(1)
        else:
            raise Exception("%s command failed" % cmdargs[0])
    return res

def connect_nbd(args):
    run_command(["modprobe", "nbd"], canfail=True)
    if args.image.endswith(".qcow2"):
        imageformat = "qcow2"
    elif args.image.endswith(".vhd"):
        imageformat = "vpc"
    else:
        print("Unknown image format, assuming VHD")
        imageformat = "vpc"

    run_command(["qemu-nbd", "-f", imageformat, "-c", "/dev/nbd%d" % args.nbddev, args.image], True)
    run_command(["udevadm", "settle"], True)
    time.sleep(1)
    return "/dev/nbd%d" % args.nbddev

def disconnect_nbd(args):
    run_command(["qemu-nbd", "-d", "/dev/nbd%d" % args.nbddev])

def connect_image(args):
    mode = os.stat(args.image).st_mode
    if not stat.S_ISBLK(mode):
        return connect_nbd(args)
    else:
        return args.image

def disconnect_image(args):
    mode = os.stat(args.image).st_mode
    if not stat.S_ISBLK(mode):
        disconnect_nbd(args)
    else:
        return

def get_partitions(imageblk):
    partitions = {"esp": None, "root": None}
    res = run_command(["lsblk", "-o", "KNAME,PARTTYPE,PARTUUID,UUID", "--json", imageblk])
    lsblk = json.loads(res.stdout)
    for part in lsblk["blockdevices"]:
        if part["parttype"] == "c12a7328-f81f-11d2-ba4b-00a0c93ec93b":
            partitions["esp"] = "/dev/%s" % part['kname']
        elif part["parttype"] == "4f68bce3-e8cd-4db1-96e7-fbcaf984b709":
            partitions["root"] = "/dev/%s" % part['kname']
        elif part["parttype"] == "8da63339-0007-60c0-c436-083ac8230908":
        # 'Linux reserved' partition at 1M offset, used for uefi variables on Azure
            partitions["efivars"] = "/dev/%s" % part['kname']

    if partitions["esp"] is None:
        raise Exception("Image doesn't contain 'EFI System' partition")
    if partitions["root"] is None:
        raise Exception("Image doesn't contain 'Linux root (x86-64)' partition")
    return partitions

def get_partnumber(root_partition):
    try:
        m = re.search(r'\d+$', root_partition)
        return m.group(0)
    except:
        return None

def encrypt(args):
    retval = 1
    imageblk = connect_image(args)
    tempdir = None
    try:
        res = run_command(["mktemp", "-d"])
        tempdir = res.stdout[:-1]

        print("Checking image partitions...")
        partitions = get_partitions(imageblk)
        root_pn = get_partnumber(partitions["root"])
        if args.growpart:
            if root_pn:
                print("Trying to grow root partition...")
                run_command(["growpart", "-v", imageblk, root_pn])
            else:
                raise Exception("Failed to find root partition number!")

        res = run_command(["blockdev", "--getsize64", partitions["root"]])
        # 32 mb for LUKS, count in kilobytes
        part2fssz = int(res.stdout) / 1024 - 32 * 1024
        print("Adjusting file system size to accommodate for LUKS metadata...")
        res = run_command(["/sbin/e2fsck", "-f", "-y", partitions["root"]], canfail=True)
        if res.returncode not in [0, 1, 2]:
            raise Exception("e2fsck failed with error code %d" % res.returncode)
        run_command(["resize2fs", partitions["root"], "%dK" % part2fssz])

        lukspw = randpw(32)
        if args.verbose:
            print("Cleartext pw is: %s" % lukspw)

        # Create cloud-init root volume resize key
        if not args.no_cloud_init:
            cloudinitpw = randpw(32)
            if args.verbose:
                print("Cloud-init root volume resize key is: %s" % cloudinitpw)

            with open(tempdir + '/cloudinitpw', 'w') as f:
                f.write(cloudinitpw)

            print("Creating cloud-init root volume resize key in /cc_growpart_keydata...")
            run_command(["mkdir", "%s/root" % tempdir])
            run_command(["mount", partitions["root"], "%s/root" % tempdir])
            with open("%s/root/cc_growpart_keydata" % tempdir, 'w') as fd:
                keydata = {"key": base64.b64encode(cloudinitpw.encode('ascii')).decode('ascii'),
                           "slot": 2}
                if args.verbose:
                    print("cloud-init root volume resize /cc_growpart_keydata data: %s" % keydata)
                fd.write(json.dumps(keydata))
            os.chmod("%s/root/cc_growpart_keydata" % tempdir, stat.S_IRUSR | stat.S_IWUSR)
            run_command(["umount", "%s/root" % tempdir])

        print("Running reencryption...")
        run_command(["cryptsetup", "reencrypt", "--encrypt", "-v", "-q", "--type", "luks2",
                     "--key-file", "-", "--luks2-metadata-size", "512k", "--luks2-keyslots-size",
                     "16384k", "--reduce-device-size", "32768k", partitions["root"]],
                    input=lukspw)
        print("Saving cleartext password as Token 0")
        token0 = {"type": "cleartext", "keyslots": ["0"], "password": lukspw}
        if args.verbose:
            print("Token 0: %s" % token0)
        run_command(["cryptsetup", "token", "import", "--token-id", "0",
                     partitions["root"] ], input=json.dumps(token0))

        # Create cloud init root volume resize keyslot
        if not args.no_cloud_init:
            print("Adding cloud init root volume resize keyslot 3")
            run_command(["cryptsetup", "luksAddKey", "-q", "--pbkdf", "pbkdf2", "--pbkdf-force-iterations", "1000",
                         "--key-file", "-", "--key-slot", "3", partitions["root"], tempdir + '/cloudinitpw'], input=lukspw)

        print("Pre-encryption done!")
        retval = 0
    except Exception as e:
        print(e)
    finally:
        if tempdir:
            run_command(["umount", "%s/root" % tempdir], canfail=True)
            run_command(["rm", "-r", "-f", tempdir], canfail=True)
        disconnect_image(args)
    return retval

def add_token_rhel(args, tempdir, partitions, pcrs, lukspw):
    port1, port2 = (None, None)

    try:
        # Run swtpm. Note, tpm2-tss supports connecting to swtpm via unix domain socked
        # since 2ed8a15cb1d7f (3.2.0). For now, don't depend on this to be able to run in
        # older environments.
        if not args.noswtpm:
            port1, port2 = get_two_free_ports()

            print("Starting swtpm on ports %d:%d" % (port1, port2))

            run_command(["swtpm", "socket", "-d", "--tpm2", "--server", "type=tcp,bindaddr=127.0.0.1,port=%d" % port1,
                         "--tpmstate",  "dir=%s" % tempdir, "--ctrl", "type=tcp,bindaddr=127.0.0.1,port=%d" % port2,
                         "--flags", "not-need-init,startup-clear"])

            # Make tpm2-tools work with swtpm
            os.environ["TPM2TOOLS_TCTI"] = "swtpm:host=127.0.0.1,port=%d" % port1

        # Pick a new password
        sealedpw = randpw(32)
        with open(tempdir + "/volume_key", "wb") as f:
            f.write(sealedpw.encode('ascii'))
        with open(tempdir + "/volume_key_b64", "wb") as f:
            f.write(base64.b64encode(sealedpw.encode('ascii')))
        with open(tempdir + "/pcrs.dat", "wb") as f:
            f.write(get_pcrs_dat(pcrs))

        if args.verbose:
            print("Volume password to be sealed: %s" % base64.b64encode(sealedpw.encode('ascii')).decode('ascii'))

        # Creare local primary key
        run_command(["tpm2_createprimary", "-C", "o", "-g", "sha256", "-G", "rsa", "-c", tempdir + "/primary.ctx"])
        # Load SRK public
        run_command(["tpm2_loadexternal", "-C", "o", "-u", args.srkpub, "-c", tempdir + "/new_parent.ctx"])
        run_command(["tpm2_readpublic", "-c", tempdir + "/new_parent.ctx", "-n", tempdir + "/new_parent.name"])
        run_command(["tpm2_flushcontext", "-t"])
        # Create PCR policy
        run_command(["tpm2_startauthsession", "-S", tempdir + "/session.dat"])
        run_command(["tpm2_policypcr", "-S", tempdir + "/session.dat", "-l", pcrs["bank"] + ":" + ",".join([str(pcr) for pcr in pcrs["pcrs"]]),
                     "-f", tempdir + "/pcrs.dat", "-L", tempdir + "/pcrpolicy.dat"])
        run_command(["tpm2_flushcontext", tempdir + "/session.dat"])
        # Create duplication policy
        run_command(["tpm2_startauthsession", "-S", tempdir + "/session.dat"])
        run_command(["tpm2_policyduplicationselect", "-S", tempdir + "/session.dat", "-N", tempdir + "/new_parent.name", "-L", tempdir + "/dpolicy.dat"])
        run_command(["tpm2_flushcontext", tempdir + "/session.dat"])
        # Create 'duplicateable' parent
        run_command(["tpm2_create", "-C", tempdir + "/primary.ctx", "-g", "sha256", "-G", "rsa", "-r", tempdir + "/parent.prv", "-u", tempdir + "/parent.pub",
                     "-c", tempdir + "/parent.ctx", "-a", "restricted|sensitivedataorigin|decrypt|userwithauth", "-L", tempdir + "/dpolicy.dat"])
        run_command(["tpm2_readpublic", "-c", tempdir + "/parent.ctx", "-n", tempdir + "/parent.name"])
        run_command(["tpm2_flushcontext", "-t"])
        # Duplicate the parent
        run_command(["tpm2_startauthsession", "--policy-session", "-S", tempdir + "/session.dat"])
        run_command(["tpm2_policyduplicationselect", "-S", tempdir + "/session.dat", "-N", tempdir + "/new_parent.name", "-n", tempdir + "/parent.name"])
        run_command(["tpm2_duplicate", "-C", tempdir + "/new_parent.ctx", "-c", tempdir + "/parent.ctx", "-G", "null", "-r", tempdir + "/parent_dup.prv", "-s",
                     tempdir + "/parent_dup.seed", "-p", "session:%s/session.dat" % tempdir])
        run_command(["tpm2_flushcontext", tempdir+"/session.dat"])
        run_command(["tpm2_flushcontext", "-t"])
        # Seal the key
        run_command(["tpm2_create", "-C", tempdir + "/parent.ctx", "-g", "sha256", "-r", tempdir + "/seal.prv", "-u", tempdir + "/seal.pub",
                     "-i", tempdir + "/volume_key", "-a", "fixedparent", "-L", tempdir + "/pcrpolicy.dat"])

        # Create new LUKS keyslot
        run_command(["cryptsetup", "luksAddKey", "-q", "--pbkdf", "pbkdf2", "--pbkdf-force-iterations", "1000", "--key-file", "-", "--key-slot", "1",
                     partitions["root"], tempdir + "/volume_key_b64"], input=lukspw)
        # Create new token (requires importing)
        token1 = {
            "type": "tpm2-import", "keyslots": ["1"],
            "tpm2-pcrs": pcrs["pcrs"], "tpm2-pcr-bank": pcrs["bank"], "tpm2-primary-alg":"rsa",
            "parent_pub": base64.b64encode(open(tempdir + "/parent.pub", "rb").read()).decode('ascii'),
            "parent_prv": base64.b64encode(open(tempdir + "/parent_dup.prv", "rb").read()).decode('ascii'),
            "parent_seed": base64.b64encode(open(tempdir + "/parent_dup.seed", "rb").read()).decode('ascii'),
            "seal_pub": base64.b64encode(open(tempdir + "/seal.pub", "rb").read()).decode('ascii'),
            "seal_prv": base64.b64encode(open(tempdir + "/seal.prv", "rb").read()).decode('ascii'),
            "pcrpolicy_dat": base64.b64encode(open(tempdir + "/pcrpolicy.dat", "rb").read()).decode('ascii')
        }
        if args.srkunique:
            token1['unique_dat'] = base64.b64encode(open(args.srkunique, "rb").read()).decode('ascii')

        if args.verbose:
            print("Token 1: %s" % token1)

        run_command(["cryptsetup", "token", "import", "--token-id", "1", partitions["root"]], input=json.dumps(token1))
    finally:
        if port2:
            run_command(["swtpm_ioctl", "--tcp", "localhost:%d" % port2, "-s"], canfail=True)

def add_token_systemd(args, tempdir, partitions, pcrs, lukspw):
    with open(tempdir + '/lukspw', 'w') as fd:
        fd.write(lukspw)
    print("Sealing root volume key with systemd-cryptenroll")
    run_command(["systemd-cryptenroll", partitions["root"], "--tpm2-device-key=" + args.srkpub,
                 "--tpm2-pcrs=" + '+'.join(str(key) + ':' + pcrs['bank'] + '=' + pcrs[str(key)] for key in pcrs['pcrs']),
                 "--unlock-key-file=" + tempdir + '/lukspw'])

def deploy(args):
    retval = 1
    tempdir = None
    imageblk = connect_image(args)
    try:
        print("Checking image partitions...")
        partitions = get_partitions(imageblk)
        print("Getting cleartext password...")
        res = run_command(["cryptsetup", "token", "export", "--token-id", "0",
                           partitions["root"] ])
        token0 = json.loads(res.stdout)
        if token0["type"] != "cleartext":
            raise Exception("LUKS token0 doesn't contain cleartext password!")
        lukspw = token0["password"]
        if args.verbose:
            print("Cleartext pw is: %s" % lukspw)

        res = run_command(["mktemp", "-d"])
        tempdir = res.stdout[:-1]

        # Mount ESP
        run_command(["mkdir", "%s/esp" % tempdir])
        run_command(["mount", partitions["esp"], "%s/esp" % tempdir])

        pcrs = {'bank': 'sha256',
                'pcrs': []}

        if args.pcr4 and args.pcr4 != 'auto':
            pcrs['pcrs'].append(4)
            pcrs['4'] = args.pcr4

        if args.pcr7 and args.pcr7 != 'auto':
            pcrs['pcrs'].append(7)
            pcrs['7'] = args.pcr7

        # Predict PCR4 value
        if args.pcr4 == 'auto':
            pcrs['4'] = predict_pcr4(args, "%s/esp" % tempdir)
            pcrs['pcrs'].append(4)
            print("Predicted PCR4 value: %s" % pcrs['4'][2:].lower())

        # Predict PCR7 value
        if args.pcr7 == 'auto':
            pcrs['7'] = predict_pcr7(args, "%s/esp" % tempdir)
            pcrs['pcrs'].append(7)
            print("Predicted PCR7 value: %s" % pcrs['7'][2:].lower())

        if args.pcrs_json:
            # Read PCR override from ESP
            print("Extracting %s PCR override from ESP..." % args.pcrs_json)

            pcrs_override = json.loads(open("%s/esp/%s" % (tempdir, args.pcrs_json), 'r').read())

            if pcrs_override['bank'] != 'sha256' and pcrs['pcrs'] != []:
                raise Exception("'bank' in the override file must be 'sha256' when --pcr4/--pcr7 are used!")

            pcrs['bank'] = pcrs_override['bank']
            for pcr in pcrs_override['pcrs']:
                print("Overriding PCR%s value with %s" % (pcr, pcrs_override[str(pcr)]))
                if not pcr in pcrs['pcrs']:
                    pcrs['pcrs'].append(pcr)
                pcrs[str(pcr)] = pcrs_override[str(pcr)]

        pcrs['pcrs'].sort()

        if (args.verbose):
            print("Expected PCR values: %s" % pcrs)

        # Check if efivars.json needs to be handled
        if "efivars" in partitions.keys():
            if os.path.exists("%s/esp/efivars.json" % tempdir):
                with open(partitions["efivars"], "wb") as f:
                    print("Writing EFI variables from efivars.json to %s" % partitions["efivars"])
                    if (args.verbose):
                        print("efivars.json size is %d" % os.stat("%s/esp/efivars.json" % tempdir).st_size)
                    f.write(ctypes.c_uint32(os.stat("%s/esp/efivars.json" % tempdir).st_size))
                    f.write(open("%s/esp/efivars.json" % tempdir, "rb").read())
            else:
                print("efivars.json not found in ESP, skipping writing EFI variables to %s" % partitions["efivars"])
        else:
            print("Can't find a partition to write EFI variables, skipping")

        # Create recovery keyslot
        if args.recovery_key:
            print("Adding recovery keyslot 4 from %s" % args.recovery_key)
            run_command(["cryptsetup", "luksAddKey", "-q", "--pbkdf", "pbkdf2", "--pbkdf-force-iterations", "1000",
                         "--key-file", "-", "--key-slot", "4", partitions["root"], args.recovery_key], input=lukspw)

        if args.seal_type in ['rhel', 'both']:
            add_token_rhel(args, tempdir, partitions, pcrs, lukspw)
        if args.seal_type in ['systemd', 'both']:
            add_token_systemd(args, tempdir, partitions, pcrs, lukspw)

        # Remove cleartext password
        run_command(["cryptsetup", "token", "remove", "--token-id", "0", partitions["root"]])
        run_command(["cryptsetup", "luksRemoveKey", "--key-file", "-", partitions["root"]], input=lukspw)

        print("Deployment done!")
        retval = 0
    except Exception as e:
        print(e)
    finally:
        if tempdir:
            run_command(["umount", "%s/esp" % tempdir], canfail=True)
            run_command(["rm", "-r", "-f", tempdir], canfail=True)
        disconnect_image(args)
    return retval

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Encrypt/deploy RHEL cloud image')
    parser.add_argument('action', choices=['encrypt', 'deploy'], help='encrypt/deploy')
    parser.add_argument('image', help='image file (VHD, QCOW2) or a block device')
    parser.add_argument('-n', '--nbddev', type=int, default=0, help='NBD device number, defaults to 0')
    parser.add_argument('-v', '--verbose', help='Print additional info', action="store_true")
    parser.add_argument('--no-cloud-init', help='Do not create /cc_growpart_keydata and LUKS keyslot for root volume resize (encrypt only)', action="store_true")
    parser.add_argument('-g', '--growpart', help='Grow root partition to the size of the volume (encrypt only)', action="store_true")
    parser.add_argument('-s', '--srkpub', help='SRK public part (deploy only)')
    parser.add_argument('-u', '--srkunique', help='SRK unique data (deploy only, `tpm2_createprimary -u` format)')
    parser.add_argument('-r', '--recovery-key', help='Recovery key file (deploy only, adds an additional passphrase to root volume)')
    parser.add_argument('--noswtpm', help='Do not use swtpm, use hardware /dev/tpm{,rm}0 instead)', action="store_true")
    parser.add_argument('--pcr4',help='Expected PCR4 sha256 value for root volume key sealing (deploy only, sha256 or "auto")')
    parser.add_argument('--pcr7', help='Expected PCR7 sha256 value for root volume key sealing (deploy only, sha256 or "auto")')
    parser.add_argument('--nosecureboot', default=False, help='Expect SecureBoot disabled (deploy only), this affects PCR4 prediction (--pcr4 auto)', action="store_true")
    parser.add_argument('--pcrs-json', help='Override specified or predicted PCR values from the specified file on the ESP (deploy only)')
    profile_group = parser.add_mutually_exclusive_group()
    profile_group.add_argument('--efivars-profile', help='UEFI profile (PK, KEK, db, dbx) efivars-format dir (e.g. /sys/firmware/efi/efivars)) for "--pcr7 auto" (deploy only)')
    profile_group.add_argument('--uefi-profile', help='UEFI profile (PK, KEK, db, dbx) JSON for "--pcr7 auto" (deploy only)')
    profile_group.add_argument('--az-disk-profile', help='Azure disk profile JSON for "--pcr7 auto" (deploy only)')
    parser.add_argument('--efivars-profile-no-attrs', help='The UEFI profile efivars-format files do not include the 4-byte attribute header (--efivars-profile only)', action="store_true")
    parser.add_argument('--seal-type', choices=['rhel', 'systemd', 'both'], default='rhel', help='Type of volume key sealing (deploy only, defaults to "rhel")')
    args = parser.parse_args()

    start = time.time()

    if args.action == 'encrypt':
        retval = encrypt(args)
    else:
        try:
            if not args.srkpub:
                raise Exception("Error: -s/--srkpub argument is mandatory for deployment!")
            if args.seal_type != 'systemd' and not args.pcrs_json and not args.pcr4 and not args.pcr7:
                raise Exception("Error: legacy sealing requires PCR values (--pcr4/--pcr7 or --pcrs-json)")
            if args.pcr4 and args.pcr4 != 'auto' and not is_sha265(args.pcr4):
                raise Exception("Error: PCR4 value must be a valid SHA256 or 'auto'")
            if args.pcr7 and args.pcr7 != 'auto' and not is_sha265(args.pcr7):
                raise Exception("Error: PCR7 value must be a valid SHA256 or 'auto'")
            if args.pcr7 == 'auto' and not any((args.efivars_profile, args.uefi_profile, args.az_disk_profile)):
                raise Exception("Error: '--efivars-profile'/'--uefi-profile'/'--az-disk-profile' must be specified for '--pcr7 auto'")
            if args.pcr7 != 'auto' and any((args.efivars_profile, args.uefi_profile, args.az_disk_profile)):
                raise Exception("Error: '--efivars-profile'/'--uefi-profile'/'--az-disk-profile' can only be used with '--pcr7 auto'")
            if args.pcr7 == 'auto' and args.nosecureboot:
                raise Exception("Error: '--nosecureboot' and '--pcr7 auto' are mutually exclusive")
        except Exception as e:
            print(e, file=sys.stderr)
            sys.exit(1)
        retval = deploy(args)

    end = time.time()

    if retval == 0:
        print("%s was successful and took %.6f seconds" % (args.action, end - start))
    else:
        print("%s failed, retval=%d" % (args.action, retval))

    sys.exit(retval)
