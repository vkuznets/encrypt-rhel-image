# Prepare RHEL/Fedora image to run with encrypted root volume

## Purpose

This tool prepares RHEL/Fedora image to run with encrypted root volume. The
key to the volume is sealed to the target TPM.

## Image pre-requisites

- GUID Partition Table (GPT)
- Image should contain ESP partition, GUID C12A7328-F81F-11D2-BA4B-00A0C93EC93B
- Image should contain 'Linux root (x86-64)' partition, GUID 4F68BCE3-E8CD-4DB1-96E7-FBCAF984B709
- Root partition should have ext4 filesystem
- The expected post boot PCR7 (sha256) value must be known, it must passed to the 'deploy' stage as '--pcr7' option or via an override file on the ESP ('--pcrs-json' option).
- Alternatively, PCR7 value can be predicted ('--pcr7 auto' option), this requires providing either UEFI profile ('--uefi-profile') or Azure Disk Profile ('--az-disk-profile'), examples are present in 'examples'.
- PCR4 can also be used in the set of PCRs for root volume key sealing. Its expected value can be specified directly ('--pcr4 sha256value') or can be predicted by passing '--pcr4 auto' option.
- ESP may contain a JSON file containing the expected PCR values in JSON format:
```
{
    "bank": "sha256",
    "pcrs": [4, 7],
    "4": "0x7A94FFE8A7729A566D3D3C577FCB4B6B1E671F31540375F80EAE6382AB785E35",
    "7": "0xB5710BF57D25623E4019027DA116821FA99F5C81E9E38B87671CC574F9281439"
}
```  
The name of the file can be passed as '--pcrs-json' option to 'deploy' stage, in that case values from
the file override '--pcr4'/'--pcr7' values from the command line. The file can specify additional PCRs.

- ESP may contain "efivars.json" file in Azure format:
```
{
    "type": "Microsoft.Compute/disks",
    "properties": {
        "uefiSettings": {
            "Boot0004": {
                "guid": "Yd/ki8qT0hGqDQDgmAMrjA==",
                "attributes": "Bw==",
                "value": "AQAAAGIAUwBoAGkAbQAgAGIAbwBvAHQAIAB0AG8AIAA1AC4AMQA0AC4AMAAtADIAMwA4AF8AdQBrAGkAXwB0AGUAcwB0ADEAOQAuAGUAbAA5AC4AeAA4ADYAXwA2ADQAAAAEASoAAgAAAAAoAAAAAAAAAOAHAAAAAABibF3EAi9J4o1TPhDbQRiuAgIEBDQAXABFAEYASQBcAHIAZQBkAGgAYQB0AFwAcwBoAGkAbQB4ADYANAAuAGUAZgBpAAAAf/8EAFwARQBGAEkAXABMAGkAbgB1AHgAXAB2AG0AbABpAG4AdQB6AC0ANQAuADEANAAuADAALQAyADMAOABfAHUAawBpAF8AdABlAHMAdAAxADkALgBlAGwAOQAuAHgAOAA2AF8ANgA0AC0AdgBpAHIAdAAuAGUAZgBpAAAA"
            }
        }
    }
}
```  

in case it does, its size and content will be written to a special 'Linux reserved'
GUID 8DA63339-0007-60C0-C436-083AC8230908) partition starting at offset 2048 * 512 = 1048576.

## TPM2

Created LUKS tokes are currently incompatible with unmodified 'systemd-cryptsetup' and require
importing. This can be done by using [tpm2-luks-import](https://gitlab.com/vkuznets/tpm2-luks-import)

SRK public key is required for 'deploy' phase. It can be obtained with

    $ tpm2_createprimary -C o -a 'restricted|decrypt|fixedtpm|fixedparent|sensitivedataorigin|userwithauth|noda' -g sha256 -G rsa -u unique.dat -c primary.ctx
    $ tpm2_readpublic -c primary.ctx -o srk.pub  

## Basic usage

    $ ./encrypt-rhel-image.py encrypt /path/to/image.vhd  
    $ ./encrypt-rhel-image.py deploy -s /path/to/srk.pub --pcr4 auto --pcr7 0x02D1E5717F514B81F8278D69D3682D88D7CD9D20AB303AB99A12C4EB38C96B89 /path/to/image.vhd  

Optionally, it is possible to pass 'unique' data to be used when creating TPM primary key,

    $ ./encrypt-rhel-image.py deploy -s /path/to/srk.pub -u /path/to/unique.dat --pcr4 auto --pcr7 0x02D1E5717F514B81F8278D69D3682D88D7CD9D20AB303AB99A12C4EB38C96B89 /path/to/image.vhd  

'unique.dat' needs to be in 'tpm2_createprimary -u' format.

## RHEL specific and systemd-cryptenroll root volume key sealing

The tool supports doing root volume key sealing in two formats: RHEL specific ('--seal-type=rhel') and
systemd-cryptenroll ('--seal-type=systemd') or both ('--seal-type=both'). RHEL specific sealing works
for RHEL>=9.2 while systemd-cryptenroll sealing can work with upstream systemd>=255.

## Running in a container

The tool can run in a container and 'Containerfile.fedora'/'Containerfile.c9s' are provided as examples to build it.
Optionally, 'quay.io/vkuznets/encrypt-rhel-image' (c9s based) can be used directly. Here is an example for Ubuntu 20.04.
Assuming all data is stored in '/data' on the host,

    # apt update && apt install docker.io  
    # docker pull quay.io/vkuznets/encrypt-rhel-image:latest  
    # modprobe nbd  
    # docker run -it --privileged --mount type=bind,source=/data,target=/data --mount type=bind,source=/run/udev,target=/run/udev --mount type=bind,source=/dev,target=/dev quay.io/vkuznets/encrypt-rhel-image:latest /usr/bin/encrypt-rhel-image.py encrypt -n 1 -v /data/image.qcow2  
    # docker run -it --privileged --mount type=bind,source=/data,target=/data --mount type=bind,source=/run/udev,target=/run/udev --mount type=bind,source=/dev,target=/dev quay.io/vkuznets/encrypt-rhel-image:latest /usr/bin/encrypt-rhel-image.py deploy -n 1 -s /data/srk_unique_le.pub -u /data/unique_le.dat -r /data/recovery_key --pcr7 auto --uefi-profile /data/uefi-profile-ovmf.json -v --seal-type=both /data/image.qcow2  

Red Hat [UBI](https://catalog.redhat.com/software/base-images) can be used for the 'deploy' phase.
As qemu-nbd is currently missing in the UBI, the image must be passed to the container as a device.
Also, UBI container can only be used for systemd-cryptenroll sealing ('--seal-type=systemd'):

    # podman build -f Containerfile.ubi9 -t encrypt-rhel-image-ubi9  
    # qemu-nbd -c /dev/nbd2 /var/lib/libvirt/images/rhel9-azure-cvm-20240820-2.qcow2  
    # podman run -it --privileged --mount type=bind,source=/var/lib/libvirt/images,target=/mnt --mount type=bind,source=/root,target=/root --mount type=bind,source=/run/udev,target=/run/udev --mount type=bind,source=/dev,target=/dev localhost/encrypt-rhel-image-ubi9:latest /usr/bin/encrypt-rhel-image.py deploy -n 1 -s /root/srk_unique_le.pub -r /root/recovery_key --pcr7 auto --uefi-profile /root/encrypt-rhel-image/examples/uefi-profile-ovmf.json -v --seal-type=systemd /dev/nbd2  
    # qemu-nbd --disconnect /dev/nbd2  

## Dependencies

Basic:
- python3.x

'Encrypt' phase:
- qemu-nbd
- e2fsprogs
- cryptsetup
- growpart (optional)

'Deploy' phase additionally requires:
- openssl

RHEL specific root volume key sealing ('--seal-type=rhel'):
- swtpm
- swtpm-tools
- tpm2-tools

Systemd-cryptenroll root volume key sealing ('--seal-type=systemd'):
- systemd-cryptenroll >= 255

TODO:
- Add support for UKI extensions ($ESP/EFI/Linux/<uki-name.efi>.extra.d/) to PCR4/7 predictors.
